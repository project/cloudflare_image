<?php

namespace Drupal\cloudflare_image_hosted_image\Plugin\media\Source;

use Drupal\cloudflare_image\Service\CloudflareImageInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media\Plugin\media\Source\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A media source plugin for hosted image assets.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "hosted_image",
 *   label = @Translation("Hosted Image"),
 *   description = @Translation("A media source plugin for hosted image assets."),
 *   allowed_field_types = {"cloudflareimage"},
 *   default_thumbnail_filename = "image.png",
 * )
 */
class HostedImageSource extends File {

  /**
   * The Cloudflare Image service.
   *
   * @var \Drupal\cloudflare_image\Service\CloudflareImageInterface
   */
  protected $cloudflareImage;

  /**
   * Constructs a HostedImageSource instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\cloudflare_image\Service\CloudflareImageInterface $cloudflare_image
   *   The Cloudflare Image service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    FieldTypePluginManagerInterface $field_type_manager,
    ConfigFactoryInterface $config_factory,
    CloudflareImageInterface $cloudflare_image
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->cloudflareImage = $cloudflare_image;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('cloudflare_image')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)->set(
      'settings',
      [
        'file_extensions' => $this->cloudflareImage->listAllowedFileExtensions(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareViewDisplay(MediaTypeInterface $type, EntityViewDisplayInterface $display) {
    $display->setComponent($this->getSourceFieldDefinition($type)->getName(), [
      'type' => 'cloudflareimage_default',
      'label' => 'visually_hidden',
    ]);
  }

}
