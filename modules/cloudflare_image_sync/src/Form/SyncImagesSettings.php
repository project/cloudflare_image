<?php

namespace Drupal\cloudflare_image_sync\Form;

use Drupal\cloudflare_image_sync\SyncImages;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for configuring the settings for the Cloudflare Image Sync module.
 */
class SyncImagesSettings extends FormBase {

  /**
   * The Cloudflare Image Sync config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The images sync service.
   *
   * @var \Drupal\cloudflare_image_sync\SyncImages
   */
  protected $syncImages;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    DateFormatterInterface $date_formatter,
    SyncImages $sync_images
  ) {
    $this->config = $config_factory->get('cloudflare_image_sync.settings');
    $this->dateFormatter = $date_formatter;
    $this->syncImages = $sync_images;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('cloudflare_image_sync')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudflare_image_sync_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Explanation.
    $form['explanation'] = [
      '#markup' => $this->t("<p>The <strong>Cloudflare Image Sync</strong> module creates a media item for each image item that is on the Cloudflare image platform.</p>
<ul><li>The <strong>initial sync</strong> will sync all images present on Cloudflare.</br>
<p>During that sync, we check whether the image exists as a media item on the website.</br>
If it doesn't, it will be imported.</p></li>
<li>The <strong>next syncs</strong> will only import new images that were added to the Cloudflare Image platform <strong>after</strong> the last sync date.</li></ul>"),
    ];

    // Show last imported timestamp as date eg. 2014-01-02T02:20:00Z
    // Set last imported timestamp.
    $timestamp = $this->config->get('last_imported');
    if (isset($timestamp)) {
      $date = $this->dateFormatter->format($timestamp, 'short');
      $form['last_imported_timestamp'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t("<strong>Last import:</strong> @date", ['@date' => $date]),
      ];
    }

    // Submit button.
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync images'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Initialize operations.
    $operations = [];

    $images = $this->syncImages->fetchImages();

    if (count($images) >= 1) {
      foreach ($images['images'] as $image) {

        $operations[] = [
          'Drupal\cloudflare_image_sync\SyncImages::syncImageCallback',
          [$image],
        ];
      }

      $batch = [
        'title' => $this->t("Synchronisation process"),
        'init_message' => $this->t("The sync process is starting."),
        'progress_message' => $this->t("Processed @current out of @total."),
        'error_message' => $this->t("The sync has encountered an error."),
        'operations' => $operations,
        'finished' => [
          'Drupal\cloudflare_image_sync\SyncImages',
          'finishedCallback',
        ],
      ];

      batch_set($batch);
    }
    else {
      $this->messenger()->addMessage($this->t('Nothing to sync.'));
    }
  }

}
