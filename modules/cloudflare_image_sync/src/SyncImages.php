<?php

namespace Drupal\cloudflare_image_sync;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\cloudflare_image\Service\CloudflareImageApiInterface;
use Drupal\cloudflare_image\Service\CloudflareImageInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\media\Entity\Media;

/**
 * Sync images service.
 *
 * @package Drupal\cloudflare_sync
 */
class SyncImages {

  /**
   * The Cloudflare Image Sync config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The Cloudflare Image service.
   *
   * @var \Drupal\cloudflare_image\Service\CloudflareImageInterface
   */
  protected $cloudflareImage;

  /**
   * The Cloudflare Image API service.
   *
   * @var \Drupal\cloudflare_image\Service\CloudflareImageApiInterface
   */
  protected $cloudflareImageApi;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    DateFormatterInterface $date_formatter,
    CloudflareImageInterface $cloudflareImage,
    CloudflareImageApiInterface $cloudflareImageApi
  ) {
    $this->config = $config_factory->getEditable('cloudflare_image_sync.settings');
    $this->dateFormatter = $date_formatter;
    $this->cloudflareImage = $cloudflareImage;
    $this->cloudflareImageApi = $cloudflareImageApi;
  }

  /**
   * Fetch image from the Cloudflare Image with an after query parameter.
   *
   * @return array
   *   An array with the result.
   */
  public function fetchImages() {
    $date = NULL;

    // Get last imported timestamp to pass to the listImages function.
    $timestamp = $this->config->get('last_imported');
    if (isset($timestamp)) {
      $date = $this->dateFormatter->format($timestamp, 'custom', 'Y-m-d\TH:m:s\Z', 'UTC');
    }

    // Get images from Cloudflare Image.
    $response = $this->cloudflareImageApi->listImages($date);

    return $response['result'];
  }

  /**
   * Sync the external images.
   *
   * @throws \Exception
   */
  public function syncImages() {
    // Fetch new images.
    $image_list = $this->fetchImages();
    if (empty($image_list)) {
      return;
    }

    // Process images.
    foreach ($image_list as $image) {
      SyncImages::processImage($image);
    }

    // Update last imported timestamp.
    $this->config->set('last_imported', time())->save();
  }

  /**
   * Process the external image.
   *
   * @param array $image
   *   The external image information.
   *
   * @return false|string
   *   False if no image is found, otherwise the filename.
   */
  public static function processImage(array $image) {
    // Get image ID.

    $imageID = SyncImages::getImageId($image);

    //foreach ($imageID as $image_id) {
    if (!SyncImages::checkIfImageIdExists($imageID)) {
      // Get filename.
      
      $filename = SyncImages::getFilename($image);
   
      // Create an empty file.
      $file = file_save_data('', 'public://' . $filename);
  
      // Create a media object of the file.      
      $media = Media::create([
        'bundle' => 'hosted_image',
        'uid' => \Drupal::currentUser()->id(),
        'langcode' => \Drupal::languageManager()->getDefaultLanguage()->getId(),
        'name' => $filename,
        'field_media_hosted_image' => [
          'target_id' => $file->id(),
          'cloudflareImageImageID' => $imageID,
          'thumbnail' => SyncImages::getThumbnail($image),
        ],
      ]);
      
      $media->setPublished(TRUE);
      $media->save();
    
      // Register file usages in the DB.
      $file_usage = \Drupal::service('file.usage');
      $file_usage->add($file, 'file', 'media', $media->id());
      $file_usage->add($file, 'cloudflare_image', 'cloudflareimage', \Drupal::currentUser()
        ->id());
      $file->setPermanent();
      $file->save();
      
      return $filename;
    }
    else {
      return FALSE;
    }
   // }
  }

  /**
   * Batch API syncImageCallback function.
   *
   * Helper function to sync the image.
   */
  public static function syncImageCallback($image , &$context) {


    if (empty($context['sandbox'])) {
      $context['sandbox'] = [];
      $context['sandbox']['progress'] = 0;
    }

    if ($filename = SyncImages::processImage($image)) {
      $context['sandbox']['progress']++;
      $context['results'][] = $filename;
      $context['message'] = t('Syncing image @filename', [
        '@filename' => $filename,
      ]);
    }
  }


  /**
   * Batch API finishedCallback function.
   *
   * Finished batch callback.
   */
  public static function finishedCallback($success, $results, $operations) {
    $messenger = \Drupal::messenger();

    if ($success) {
      // Set last imported timestamp.
      $config = \Drupal::service('config.factory')
        ->getEditable('cloudflare_image_sync.settings');
      $config->set('last_imported', time())->save();

      $message = \Drupal::translation()->formatPlural(
        count($results),
        t("One image processed."), t("@count images processed.")
      );
      $messenger->addMessage($message);
    }
    else {
      $error_operation = reset($operations);
      $messenger->addMessage(
        t("An error occurred while processing @operation with arguments : @args",
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

  /**
   * Return the filename from the image.
   *
   * @param array $image
   *   The external image information.
   *
   * @return string
   *   The filename.
   */
  protected static function getFilename(array $image) {
    return $image['filename'];
  }

  /**
   * Return the image ID from the image.
   *
   * @param array $image
   *   The external image information.
   *
   * @return string
   *   The image ID.
   */
  protected static function getImageId(array $image) {
    return $image['id'];
  }

  /**
   * Return the thumbnail from the image.
   *
   * @param array $image
   *   The external image information.
   *
   * @return string
   *   The thumbnail uri.
   */
  protected static function getThumbnail(array $image) {
    return $image['variants'][0];
  }

  /**
   * Check if ImageID already exists.
   *
   * @param string $imageID
   *   The image ID.
   *
   * @return bool
   *   True if image exists, false otherwise.
   */
  protected static function checkIfImageIdExists(string $imageID) {  

    //$imageID = $imageID[0]['id'];
    $query = \Drupal::database()
      ->select('media__field_media_hosted_image', 'media_cv');
    $query->fields('media_cv');
    $query->condition('field_media_hosted_image_cloudflareImageImageID', $imageID);
    $results = $query->execute()->fetchAll();

  
    if (count($results) === 0) {
      return FALSE;
    }

    return TRUE;
  }

}
