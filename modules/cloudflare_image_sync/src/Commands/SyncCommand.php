<?php

namespace Drupal\cloudflare_image_sync\Commands;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\cloudflare_image_sync\SyncImages;
use Drush\Commands\DrushCommands;

/**
 * Sync Drush Command.
 */
class SyncCommand extends DrushCommands {

  /**
   * The actual sync process.
   *
   * @var \Drupal\cloudflare_image_sync\SyncImages
   */
  protected $sync;

  /**
   * SyncCommand constructor.
   *
   * @param \Drupal\cloudflare_image_sync\SyncImages $sync_images
   *   Synchronizer.
   */
  public function __construct(
    SyncImages $sync_images
  ) {
    $this->sync = $sync_images;
  }

  /**
   * Perform the Cloudflare Image image sync.
   *
   * @command cloudflareimage:sync
   * @aliases css
   * @usage cloudflareimage:sync
   */
  public function import() {
    try {
      $this->sync->syncImages();
    }
    catch (\Exception $exception) {

      $message = new FormattableMarkup(
        '@class (@function): sync error. Error details are as follows:<pre>@response</pre>',
        [
          '@class' => get_class(),
          '@function' => __FUNCTION__,
          '@response' => print_r($exception->getMessage(), TRUE),
        ]
      );

      // Log the error.
      watchdog_exception('cloudflare_image_sync', $exception, $message);
    }
  }

}
