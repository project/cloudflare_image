Cloudflare Image
----------------

Cloudflare Image module integrates new Cloudflare Image functionality of syncing the images from Drupal to Cloudflare and vice versa. The working of this module is based on the Cloudflare Stream module but it leverage Cloudflare Image APIs.