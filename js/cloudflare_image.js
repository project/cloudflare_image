(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.cloudflareImage = {
    attach: function (context, settings) {
      let uniqueImageIDs = drupalSettings.cloudflare_image;

      // Loop through all Cloudflare Images.
      $.each(uniqueImageIDs, function (key, settings) {
        let image = $('#' + key + '> image');
        const muted = settings.muted;
        const controls = settings.controls;
        const autoplay = settings.autoplay;
        const loop = settings.loop;
        const width = settings.width;
        const height = settings.height;

        // Adding attributes to image image.
        image.attr({'preload':""});
        if (controls) {
          image.prop(controls, true);
          $('.image-js').removeClass('vjs-controls-disabled').addClass('vjs-controls-enabled');
        }
        if (muted) {
          image.attr(muted, true);
        }
        if (autoplay) {
          image.prop(autoplay, true);
        }
        if (loop) {
          image.prop("loop", true);
        }
        image.css('width', width);
        image.css('height', height);
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
