<?php

namespace Drupal\cloudflare_image\Plugin\Field\FieldType;

use Drupal\cloudflare_image\Service\CloudflareImageApiInterface;
use Drupal\cloudflare_image\Service\CloudflareImageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TraversableTypedDataInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\file\Plugin\Field\FieldType\FileItem;

/**
 * Plugin implementation of the 'cloudflareImage' field type.
 *
 * @FieldType(
 *   id = "cloudflareimage",
 *   label = @Translation("Cloudflare Image"),
 *   description = @Translation("This field stores the ID of an cloudflare image."),
 *   category = @Translation("Cloudflare"),
 *   default_widget = "cloudflareimage_default",
 *   default_formatter = "cloudflareimage_default",
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 *   constraints = {"ReferenceAccess" = {}, "FileValidation" = {}}
 * )
 */
class CloudflareImageItem extends FileItem {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The Cloudflare Image service.
   *
   * @var \Drupal\cloudflare_image\Service\CloudflareImageInterface
   */
  protected $cloudflareImage;

  /**
   * The Cloudflare Image API service.
   *
   * @var \Drupal\cloudflare_image\Service\CloudflareImageApiInterface
   */
  protected $cloudflareImageApi;

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    DataDefinitionInterface $definition,
    EntityTypeManagerInterface $entity_type_manager,
    FileSystemInterface $file_system,
    CloudflareImageInterface $cloudflareImage,
    CloudflareImageApiInterface $cloudflareImageApi,
    $name = NULL,
    TypedDataInterface $parent = NULL,
    FileUsageInterface $file_usage = NULL
  ) {
    parent::__construct($definition, $name, $parent);
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->fileUsage = $file_usage;
    $this->cloudflareImage = $cloudflareImage;
    $this->cloudflareImageApi = $cloudflareImageApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance($definition, $name = NULL, TraversableTypedDataInterface $parent = NULL) {
    $entityTypeManager = \Drupal::service('entity_type.manager');
    $fileSystem = \Drupal::service('file_system');
    $fileUsage = \Drupal::service('file.usage');
    $cloudflareImage = \Drupal::service('cloudflare_image');
    $cloudflareImageApi = \Drupal::service('cloudflare_image.api');
    return new static(
      $definition,
      $entityTypeManager,
      $fileSystem,
      $cloudflareImage,
      $cloudflareImageApi,
      $name,
      $parent,
      $fileUsage
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $fieldSettings = parent::defaultFieldSettings();
    $cloudflareImage = \Drupal::service('cloudflare_image');
    $fieldSettings['file_extensions'] = $cloudflareImage->listAllowedFileExtensions();

    return $fieldSettings;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['cloudflareImageImageID'] = [
      'type' => 'text',
      'length' => 512,
      'not null' => FALSE,
    ];
    $schema['columns']['thumbnail'] = [
      'type' => 'text',
      'length' => 512,
      'not null' => FALSE,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);

    $form['file_extensions']['#description'] = $this->t('Separate extensions with a space or comma and do not include the leading dot.<br>Cloudflare Image supports following extensions: MP4, MKV, MOV, AVI, FLV, MPEG-2 TS, MPEG-2 PS, MXF, LXF, GXF, 3GP, WebM, MPG, QuickTime');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['cloudflareImageImageID'] = DataDefinition::create('string')
      ->setLabel(t('Cloudflare Image ID'));
    $properties['thumbnail'] = DataDefinition::create('string')
      ->setLabel(t('Cloudflare Image thumbnail'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    $entity = $this->getEntity();

    if (is_null($this->getValue()['cloudflareImageImageID'])) {
      $fileID = $this->get('target_id')->getValue();
      $file = $this->entityTypeManager->getStorage('file')->load($fileID);

      if (isset($file)) {
    
        $response = $this->cloudflareImageApi->uploadImageByHttpRequest($file);

        if (isset($response['result']['id'])) {
          $imageId = $response['result']['id'];
          $oldImageId = $this->getOldImageId($entity, $this->name);
          if (isset($oldImageId)) {
            $this->cloudflareImageApi->deleteImage($oldImageId);
          }
          $this->setValue(
            [
              'target_id' => $this->getValue()['target_id'],
              'display' => $this->getValue()['display'],
              'description' => $this->getValue()['description'],
              'cloudflareImageImageID' => $imageId,
              'thumbnail' => $response['result']['variants'],
            ]
          );
          // Override file contents, so the original image doesn't get stored on
          // the website.
          if ($this->cloudflareImage->replaceSourceFiles()) {
            file_put_contents($this->fileSystem->realpath($file->getFileUri()), '');
          }
          $this->fileUsage->add($file, 'cloudflare_image', 'cloudflareimage', \Drupal::currentUser()->id());
          $file->setPermanent();
          $file->save();
        }
      }
    }
    else {
      $fieldName = $this->getParent()->getName();
      $newFieldData = $this->getEntity()->get($fieldName)->getValue();
      $originalFieldData = $entity->get($fieldName)->getValue();
      $imageIds = array_filter($this->getRemovedIds($originalFieldData, $newFieldData), function ($element) {
        if ($element !== NULL) {
          return $element;
        }
      });

      foreach ($imageIds as $imageId) {
        $this->cloudflareImageApi->deleteImage($imageId);
      }
    }
  }

  /**
   * Return the old image ID.
   */
  protected function getOldImageId($entity, $delta) {
    $oldImageID = NULL;
    if ($entity) {
      $fieldName = $this->getParent()->getName();
      $oldImageID = $entity->get($fieldName)->getValue()[$delta]['cloudflareImageImageID'] ? $entity->get($fieldName)->getValue()[$delta]['cloudflareImageImageID'] : NULL;
    }
    return $oldImageID;
  }

  /**
   * Get the removed image IDs from cloudflare image.
   *
   * @param array $originalFieldData
   *   Contains the original image data.
   * @param array $newFieldData
   *   Contains the new image data.
   *
   * @return array
   *   The removed image IDs.
   */
  protected function getRemovedIds(array $originalFieldData, array $newFieldData) {
    $removedIds = [];
    foreach ($originalFieldData as $key => $originalFieldDatum) {
      foreach ($newFieldData as $newFieldDatum) {
        if ($originalFieldDatum['cloudflareImageImageID'] === $newFieldDatum['cloudflareImageImageID']) {
          $removedIds[$key] = NULL;
          break;
        }
        $removedIds[$key] = $originalFieldDatum['cloudflareImageImageID'];
      }
    }

    return $removedIds;
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    if (!empty($this->getValue()['cloudflareImageImageID'])) {
      $this->cloudflareImageApi->deleteImage($this->getValue()['cloudflareImageImageID']);
    } 
  }

}
