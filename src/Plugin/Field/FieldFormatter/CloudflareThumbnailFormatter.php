<?php

namespace Drupal\cloudflare_image\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'cloudflarethumbnail_default' formatter.
 *
 * @FieldFormatter(
 *   id = "cloudflarethumbnail_default",
 *   module = "cloudflare_image",
 *   label = @Translation("Cloudflare Thumbnail"),
 *   field_types = {
 *     "cloudflareimage"
 *   }
 * )
 */
class CloudflareThumbnailFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Shows thumbnail of the image.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    echo "<pre>";
    echo "test";
    print_r($items);
    exit;
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'container',
        '#markup' => "<img src=‌‌{$item->getValue()['thumbnail']} loading='lazy' />",
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    $elements = $this->viewElements($items, $langcode);
    if (!isset($elements) || is_null($elements)) {
      return [];
    }
    return parent::view($items, $langcode);
  }

}
