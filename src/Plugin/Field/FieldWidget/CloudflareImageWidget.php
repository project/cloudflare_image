<?php

namespace Drupal\cloudflare_image\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;

/**
 * Plugin implementation of the 'Cloudflareimage' widget.
 *
 * @FieldWidget(
 *   id = "cloudflareimage_default",
 *   module = "cloudflare_image",
 *   label = @Translation("Cloudflare Image"),
 *   field_types = {
 *     "cloudflareimage"
 *   }
 * )
 */
class CloudflareImageWidget extends FileWidget {

  /**
   * {@inheritdoc}
   */
  public static function process($element, FormStateInterface $form_state, $form) {
    $element = parent::process($element, $form_state, $form);

    // Add own description to the description field.
    if (isset($element['description']['#description'])) {
      $element['description']['#description'] = t('The description will be shown below the image.');
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function value($element, $input, FormStateInterface $form_state) {
    $return = parent::value($element, $input, $form_state);
    $return['cloudflareImageImageID'] = $element['#default_value']['cloudflareImageImageID'] ?? NULL;
    $return['thumbnail'] = $element['#default_value']['thumbnail'] ?? NULL;

    return $return;
  }

}
