<?php

namespace Drupal\cloudflare_image\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures the settings for Cloudflare Image module.
 */
class CloudflareImageSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cloudflare_image.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudflare_image_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Getting already saved config from the database.
    $api_token = $this->config('cloudflare_image.settings')->get('api_token');
    $account_id = $this->config('cloudflare_image.settings')->get('account_id');
    $zone_id = $this->config('cloudflare_image.settings')->get('zone_id');
    $replace_source_files = $this->config('cloudflare_image.settings')->get('replace_source_files');
    $debug_messages = $this->config('cloudflare_image.settings')->get('debug_messages');

    // Adding form fields to the form.
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Token'),
      '#description' => $this->t('To find your API Token go to the Cloudflare Dashboard, optionally select your account, and select the website. In the sidebar API section you can find a link to the API Token page where you can generate an API token.'),
      '#default_value' => $api_token,
      '#required' => TRUE,
    ];
    $form['website_specific_settings'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Website specific settings'),
      'instruction' => [
        '#markup' => '<p>' . $this->t('The Account & Zone ID are website specific. To find these you go to the Cloudflare Dashboard, optionally select your account, and select the website. The needed info can be found under the API section in the sidebar.') . "</p>",
      ],
    ];
    $form['website_specific_settings']['zone_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zone ID'),
      '#default_value' => $zone_id,
      '#required' => TRUE,
    ];
    $form['website_specific_settings']['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account ID'),
      '#default_value' => $account_id,
      '#required' => TRUE,
    ];
    $form['replace_source_files'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Do not store the source files locally'),
      '#description' => $this->t("Check this box if you don't want the source files stored on the webserver. The image file will be replaced with an empty file when saved."),
      '#default_value' => $replace_source_files,
    ];
    $form['debug_messages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show debug messages'),
      '#description' => $this->t("Check this box if you want to show debug messages in the frontend. By default, we only log the error messages in watchdog."),
      '#default_value' => $debug_messages,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Trimming user entry for any whitespace.
    $api_token = preg_replace('/\s/', '', $form_state->getValue('api_token'));
    $account_id = preg_replace('/\s/', '', $form_state->getValue('account_id'));
    $zone_id = preg_replace('/\s/', '', $form_state->getValue('zone_id'));
    $replace_source_files = $form_state->getValue('replace_source_files');
    $debug_messages = $form_state->getValue('debug_messages');

    // Saving config in the database.
    $this->config('cloudflare_image.settings')
      ->set('api_token', $api_token)
      ->set('account_id', $account_id)
      ->set('zone_id', $zone_id)
      ->set('replace_source_files', $replace_source_files)
      ->set('debug_messages', $debug_messages)
      ->save();
    $this->messenger()->addMessage($this->t('Cloudflare configurations saved successfully.'));
  }

}
