<?php

namespace Drupal\cloudflare_image\Service;

/**
 * Interface CloudflareImageInterface.
 *
 * Describes the Cloudflare Image service functions.
 */
interface CloudflareImageInterface {

  /**
   * Whether or not the debug is enabled.
   *
   * @return bool
   *   True if a debug is enabled, false otherwise.
   */
  public function debugEnabled();

  /**
   * Returns the api_token from Cloudflare Image configuration.
   * @return string
   *   The API token.
   */
  public function getApiToken();

  /**
   * Returns the zone ID from Cloudflare Image configuration.
   *
   * @return string
   *   The zone ID.
   */
  public function getZoneId();

  /**
   * Returns the account ID from Cloudflare Image configuration.
   *
   * @return string
   *   The account ID.
   */
  public function getAccountId();

  /**
   * Whether or not to replace the source files.
   *
   * @return bool
   *   True if source files needs to be replaced, false otherwise.
   */
  public function replaceSourceFiles();

  /**
   * Lists all allowed file extensions.
   *
   * @return string
   *   The allowed file extensions.
   */
  public function listAllowedFileExtensions();

}
