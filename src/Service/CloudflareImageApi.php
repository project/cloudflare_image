<?php

namespace Drupal\cloudflare_image\Service;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Provides a CloudflareImage API service class.
 */
class CloudflareImageApi implements CloudflareImageApiInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The Cloudflare Image service.
   *
   * @var \Drupal\cloudflare_image\Service\CloudflareImageInterface
   */
  protected $cloudflareImage;

  /**
   * Http timeout time.
   *
   * @var int
   */
  protected $timeout;

  /**
   * CloudflareImageApi constructor.
   *
   * @param \GuzzleHttp\Client $client
   *   The HTTP client.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Drupal\cloudflare_image\Service\CloudflareImageInterface $cloudflareImage
   *   The Cloudflare Image service.
   */
  public function __construct(
    Client $client,
    MessengerInterface $messenger,
    FileSystemInterface $fileSystem,
    CloudflareImageInterface $cloudflareImage
  ) {
    $this->client = $client;
    $this->messenger = $messenger;
    $this->fileSystem = $fileSystem;
    $this->cloudflareImage = $cloudflareImage;
    $this->timeout = 600;
  }

  /**
   * {@inheritDoc}
   */
  public function uploadImageByHttpRequest(EntityInterface $file) {
    $fileRealPath = $this->fileSystem->realpath($file->getFileUri());
    
    $filename = $file->getFilename();
 
    try {
      $request = $this->client->post($this->getApiUrl(), [
        'timeout' => $this->getTimeout(),
        'headers' => $this->getHeaders(),
        'multipart' => [
          [
            'name' => 'file',
            'contents' => file_get_contents($fileRealPath),
            'filename' => $filename,
          ],
        ],
      ]);

      return Json::decode($request->getBody()->getContents());
    }
    catch (GuzzleException $error) {
      $this->handleError($error, __FUNCTION__);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getEmbedCodeHtml(string $identifier) {

    if(!empty($identifier)) {
      
      try {
        $headers = $this->getHeaders();
        $headers['Content-Type'] = 'application/json';

        return $this->client->get("{$this->getApiUrl()}/$identifier", [
          'headers' => $headers,
        ]);

      }
      catch (GuzzleException $error) {
        $this->handleError($error, __FUNCTION__);
      }

    }
  }

  /**
   * {@inheritDoc}
   */
  public function deleteImage(string $identifier) {
    try {
      $headers = $this->getHeaders();
      $headers['Content-Type'] = 'application/json';

      return $this->client->delete("{$this->getApiUrl()}/$identifier", [
        'headers' => $headers,
      ]);

    }
    catch (GuzzleException $error) {
      $this->handleError($error, __FUNCTION__);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function listImages(string $after = NULL, string $before = NULL, bool $include_counts = FALSE, string $search = NULL, int $limit = NULL, bool $asc = FALSE, string $status = NULL) {
    try {
      $headers = $this->getHeaders();
      $headers['Content-Type'] = 'application/json';
      $queryParameters = [];
      $allowedQueryParameters = [
        'after',
        'before',
        'include_counts',
        'search',
        'limit',
        'asc',
        'status',
      ];


      foreach ($allowedQueryParameters as $key) {
        if (${$key}) {
          $queryParameters[$key] = ${$key};
        }
      }

      $request = $this->client->get("{$this->getApiUrl()}", [
        'headers' => $headers,
        'query' => $queryParameters,
      ]);
 
      return Json::decode($request->getBody()->getContents());
    }
    catch (GuzzleException $error) {
      $this->handleError($error, __FUNCTION__);
    }
  }

  /**
   * Get the API URL.
   *
   * @return string
   *   The URL.
   */
  protected function getApiUrl() {
    return "https://api.cloudflare.com/client/v4/accounts/{$this->cloudflareImage->getAccountId()}/images/v1";
  }

  /**
   * Return the headers for the API request.
   */
  protected function getHeaders() {
    return [
      'Authorization' => 'Bearer ' . $this->cloudflareImage->getApiToken(),
    ];
  }

  /**
   * Return the timeout time.
   */
  protected function getTimeout() {
    return $this->timeout;
  }

  /**
   * Handles the error message.
   *
   * Using FormattableMarkup allows for the use of <pre/> tags, giving a more
   * readable log item.
   *
   * @param \GuzzleHttp\Exception\GuzzleException $error
   *   The error.
   * @param string $function
   *   The function that calls the function.
   */
  protected function handleError(GuzzleException $error, string $function) {
    // Get the original response.
    $response = $error->getResponse();

    // Get the info returned from the remote server.
    $response_info = Json::decode($response->getBody()->getContents());

    $message = new FormattableMarkup(
      '@class (@function): API connection error. Error details are as follows:<pre>@response</pre>',
      [
        '@class' => get_class(),
        '@function' => $function,
        '@response' => print_r($response_info['errors'], TRUE),
      ]
    );

    // Log the error.
    watchdog_exception('cloudflare_image', $error, $message);

    // Show the error in the frontend if debug message option is enabled.
    if ($this->cloudflareImage->debugEnabled()) {
      $this->messenger->addError($message);
    }
  }

}
