<?php

namespace Drupal\cloudflare_image\Service;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class CloudflareImage.
 *
 * The Cloudflare Image service.
 */
class CloudflareImage implements CloudflareImageInterface {

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a CloudflareImage object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('cloudflare_image.settings');
  }

  /**
   * {@inheritDoc}
   */
  public function debugEnabled() {
    return $this->config->get('debug_messages');
  }

  /**
   * {@inheritDoc}
   */
  public function getApiToken() {
    return $this->config->get('api_token');
  }

  /**
   * {@inheritDoc}
   */
  public function getZoneId() {
    return $this->config->get('zone_id');
  }

  /**
   * {@inheritDoc}
   */
  public function getAccountId() {
    return $this->config->get('account_id');
  }

  /**
   * {@inheritDoc}
   */
  public function replaceSourceFiles() {
    return $this->config->get('replace_source_files');
  }

  /**
   * {@inheritDoc}
   */
  public function listAllowedFileExtensions() {
    return 'jpg png jpeg';
  }

}
