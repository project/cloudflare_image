<?php

namespace Drupal\cloudflare_image\Service;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an interface for the CloudflareImage API service.
 */
interface CloudflareImageApiInterface {

  /**
   * Upload a image using a single HTTP request.
   *
   * @param \Drupal\Core\Entity\EntityInterface $file
   *   The image file.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response from the Cloudflare Image.
   */
  public function uploadImageByHttpRequest(EntityInterface $file);

  /**
   * Get the embed code HTML for a image on Cloudflare Image.
   *
   * @param string $identifier
   *   The identifier of the image.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response from the Cloudflare Image.
   */
  public function getEmbedCodeHtml(string $identifier);

  /**
   * Delete a image on Cloudflare Image.
   *
   * @param string $identifier
   *   The identifier of the image.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response from the Cloudflare Image.
   */
  public function deleteImage(string $identifier);

  /**
   * List up to 1000 images in one request.
   *
   * @param string|null $after
   *   Show images created after this date-time.
   * @param string|null $before
   *   Show images created before this time.
   * @param bool $include_counts
   *   Include stats in the response about the number of images in response
   *   range and total number of images available.
   * @param string|null $search
   *   A string provided in this field will be used to search over the 'name'
   *   key in meta field, which can be set with the upload request of after.
   * @param int|null $limit
   *   Number of images to include in the response.
   * @param bool $asc
   *   List images in ascending order of creation.
   * @param string|null $status
   *   Filter by statuses.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response from the Cloudflare Image.
   */
  public function listImages(string $after = NULL, string $before = NULL, bool $include_counts = FALSE, string $search = NULL, int $limit = NULL, bool $asc = FALSE, string $status = NULL);

}
